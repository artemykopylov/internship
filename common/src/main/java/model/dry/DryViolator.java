package internship.dry;

public class DryViolator {
    public boolean ifEvenSayHello(int num){
        if(isaBoolean(num))
            System.out.println("Hello");

        return (isaBoolean(num));
    }

    private boolean isaBoolean(int num) {
        return (num % 2) == 0;
    }
}
