package model.firstTask;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@AllArgsConstructor
@Data
public class Human {
    private String name, lastName, patronymicName;
    Short age;
    Date birthDate;
    Boolean sex;

}
