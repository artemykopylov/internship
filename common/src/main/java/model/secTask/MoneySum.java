package model.secTask;


public interface MoneySum {

    Double howMuch();

    MoneySum.Currency getCurrency();

    public enum Currency{
        USD,
        EUR,
        RUB;
    }

}
