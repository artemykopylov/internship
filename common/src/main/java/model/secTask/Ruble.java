package model.secTask;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;


@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Ruble implements MoneySum{

    private Double amount;

    private final MoneySum.Currency currencyUnit = Currency.RUB;

    public Double howMuch() {
        return amount;
    }

    public Currency getCurrency() {
        return currencyUnit;
    }
}
