package model.secTask;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Dollar implements MoneySum{



    private final MoneySum.Currency currencyUnit = Currency.USD;
    private Double amount;

    public Double howMuch() {
        return amount;
    }

    public MoneySum.Currency getCurrency() {
        return currencyUnit;
    }
}
