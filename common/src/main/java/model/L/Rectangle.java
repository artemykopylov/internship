package internship.L;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Rectangle {

    @Getter
    @Setter
    int a, b;

    public Rectangle() {
    }

    public int getPerimeter(){
        return a * 2 + b * 2;
    }
}
