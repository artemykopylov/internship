package internship.L;

import lombok.Getter;
import lombok.Setter;


public class Square extends Rectangle{

    @Getter
    @Setter
    int a;

    public Square(int a){
        super(a, a);
    }

}
