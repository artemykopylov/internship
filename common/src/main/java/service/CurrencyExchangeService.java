package service;

import model.secTask.Dollar;
import model.secTask.Euro;
import model.secTask.MoneySum;
import model.secTask.Ruble;

public class CurrencyExchangeService {
    private static final Double RUBTOUSD = 0.013;
    private static final Double RUBTOEUR = 0.011;

    private static final Double USDTORUB = 77.21;
    private static final Double USDTOEUR = 0.84;

    private static final Double EURTOUSD = 1.19;
    private static final Double EURTORUB = 91.63;


    /** Method exchanges money to a different currency by using fixed currency rate.
     *
     * @param moneySum money which will be exchanged
     * @param currency enu, of currency, for which you need to echange your money (USD, EUR, RUB)
     * @return exhanged amount of money
     */
    public static MoneySum exchange(MoneySum moneySum, MoneySum.Currency currency){

        Double value = moneySum.howMuch();

         if (moneySum.getCurrency().equals(MoneySum.Currency.RUB)){
             switch(currency){
                 case USD: return new Dollar(value * RUBTOUSD);
                 case EUR: return new Euro(value * RUBTOEUR);
             }
         }

         if(moneySum.getCurrency().equals(MoneySum.Currency.EUR)){
            switch (currency){
                case RUB: return new Ruble(value * EURTORUB);
                case USD: return new Dollar(value * EURTOUSD);
            }
         }

         if(moneySum.getCurrency().equals(MoneySum.Currency.USD)){
            switch (currency){
                case RUB: return new Ruble(value * USDTORUB);
                case EUR: return new Euro(value * USDTOEUR);
            }
         }
    return moneySum;
    }
}
