package service;

import model.firstTask.Human;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class HumanService {

    private static List<Human> humansShablon = new ArrayList<Human>();

    static {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            humansShablon.add(new Human("Roman", "Roman", "Roman", (short) 19, simpleDateFormat.parse("20-08-1993"), true));
            humansShablon.add(new Human("Denis", "Denis", "Denis", (short) 21, simpleDateFormat.parse("20-08-1993"), true));
            humansShablon.add(new Human("Noname", "Noname", "Noname", (short) 21, simpleDateFormat.parse("20-08-1993"), true));
            humansShablon.add(new Human("Phill", "Phill", "Phill", (short) 21, simpleDateFormat.parse("20-08-1993"), true));
            humansShablon.add(new Human("Semen", "Semen", "Semen", (short) 12, simpleDateFormat.parse("20-08-1993"), true));
            humansShablon.add(new Human("Petr", "Petr", "Petr", (short) 21, simpleDateFormat.parse("20-08-1993"), true));
            humansShablon.add(new Human("Petr", "Petr", "Petr", (short) 21, simpleDateFormat.parse("20-08-1993"), true));
            humansShablon.add(new Human("Artem", "Artem", "Artem", (short) 21, simpleDateFormat.parse("20-08-1993"), true));
            humansShablon.add(new Human("Alex", "Alex", "Alex", (short) 21, simpleDateFormat.parse("20-08-1993"), true));
            humansShablon.add(new Human("Alex", "Alex", "Alex", (short) 21, simpleDateFormat.parse("20-08-1993"), true));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**Method is searching for the full coincedences of input human with created humans in prepared shablon.
     *
     * @param human for which method is searching dor the repetitions
     * @author A.Kopylov
     */
    public static void doesCoincidenceExist(Human human) {
        int coincedences = 0;
        for (Human shablon :
                humansShablon) {

            if (shablon.equals(human))
                coincedences++;


        }
        System.out.println("there are " + coincedences + " coincedences");
    }

    /**Method is searching in prepared shablon for humans with age not more then 20, and humans with last name starts with a, b, c, d, e.
     *@Author A.Kopylov
     */
    public static void ShowPeopleYounger20andOthers() {

        Set<Human> result = new HashSet<Human>();

        for (Human shablon :
                humansShablon) {

            if (shablon.getLastName().startsWith("a") || shablon.getLastName().startsWith("b")
                    || shablon.getLastName().startsWith("c") || shablon.getLastName().startsWith("d")
                    || shablon.getLastName().startsWith("e") || shablon.getAge() <= 20) {
                result.add(shablon);
            }
        }

        System.out.println(result);


    }

    /**Method is searching for the coincedences in shablon and show it in terminal, if there are one or more repetitions
     * @Author A.Kopylov
     */

    public static void repetitionsInShablon(){


        for (int i = 0; i < humansShablon.size(); i++) {
            Map <Integer, Human> repetitions = new HashMap<Integer, Human>();
            int rep = 1;

            for (int j = i+1; j < humansShablon.size(); j++) {
                if(humansShablon.get(i).equals(humansShablon.get(j)))
                    rep++;
            }

            boolean thisHumanWasntBefore = true;

            for (int j = 0; j < i; j++) {
                if (humansShablon.get(i).equals(humansShablon.get(j))){
                    thisHumanWasntBefore = false;

                }
            }
            if(thisHumanWasntBefore && rep > 1) {
                repetitions.put(rep, humansShablon.get(i));
                System.out.println(repetitions);
            }

        }

    }
}

