# README #

Here you can find Task#1 and Task#2 from Day 2 Internship

All the model objects (Human and Money) are stored in common/src/main/java/model.
All service object (those who perform business logic) are stored in common/src/main/java/service.

Main method, whick kicks off the application performing is located in core/src/main/java/Main.java.