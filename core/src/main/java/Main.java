import model.firstTask.Human;
import model.secTask.Dollar;
import model.secTask.MoneySum;
import model.secTask.Ruble;
import service.CurrencyExchangeService;
import service.HumanService;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) throws ParseException {
        //Need this one to create a sample Human
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        HumanService.repetitionsInShablon();

        HumanService.doesCoincidenceExist(new Human("Alex", "Alex", "Alex", (short) 21, simpleDateFormat.parse("20-08-1993"), true));

        HumanService.ShowPeopleYounger20andOthers();


        Dollar dollar = new Dollar(300.00);
        Ruble ruble = (Ruble) CurrencyExchangeService.exchange(dollar, MoneySum.Currency.RUB);

        System.out.println("you exchanged " + dollar.howMuch() + " " + dollar.getCurrency() + " to " + ruble.howMuch() +
                " " + ruble.getCurrency());

    }
}
